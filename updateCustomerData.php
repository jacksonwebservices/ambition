<?php

// @author: Hunter Norman
// @last updated: 09/18/2017

/*
The purpose of this script is to run in sequence with several other php scripts that are used for updating our database server with real-time information from our phone system database. However, the script will solely serve as the means of updating customer info in our database with updated customer info from our DB2 database on the AS400 server. All other scripts work between two different MYSQL connections while this is the only one dealing directly with the DB2 connection. This script will run first in the sequence followed by updatePhoneTables.php, ambitionTestScript.php and runAmbitionReports.php
*/


//MySql Connection credentials
$mysqlServer = "192.168.20.11";
$mysqlUser = "normanh";
$mysqlPass = "Guitar138*";

//Establilsh MySql Connection
$mysqlConn = new mysqli($mysqlServer, $mysqlUser, $mysqlPass);

//Check MySQL connection
if($mysqlConn->connect_error){
  die("Connection Failed: " .$mysqlConn->connect_error);
}
echo "Connected Succssfully to Mysql";

//Establish DB2 connection
$DB2Conn = odbc_connect("JFIWeblink","webuser", "jfiweb");

//Check DB2 Connection
if(!$DB2Conn){
  die("Could not connect");
}
echo "Connected Succssfully to DB2";



/*
/
/
//////QUERIES BELOW
/
/
*/



//Query for selecting records from jacmfg.custmstf on DB2
$query1 = "select xcompn, xcstno, xbllnm, xcntpc, xcntec from jacmfg.custmstf";
$prep1 = odbc_prepare($DB2Conn, $query1);
$exec1 = odbc_execute($prep1);

//If either query fails
if (!$exec1) {

  die('Could not update.');
}
else {
echo 'success';

}


//Query for inserting new customer records into ambition.ambition_customer_data or updating upon full duplicate
$stmt = mysqli_prepare($mysqlConn,
        "INSERT into ambition.ambition_customer_data(company_number, customer_number, business_name,
        phone, email )
          VALUES (?, ?, ?, ?, ?)
          ON duplicate key update
             phone = values(phone),
             email = values(email)") or die(mysqli_error( $mysqlConn));

//Execute insert/update statement for customer data
mysqli_stmt_execute($stmt) or die(mysqli_error($mysqlConn));






//Following queries will execute the Same process but for sales phonebook instead of customers

//Select From sales phonebook
$query2 = "select compnp,slsnop,slsfnp, slslnp,slssnp, slscel, slshom, slswrk, slseml from jacmfg.salphbpf";
$prep2 = odbc_prepare($DB2Conn, $query2);
$exec2 = odbc_execute($prep2);

//If either query fails
if (!$exec2) {

  die('Could not update.');
}
else {
echo 'success';
}


//Query for inserting new customer records into ambition.ambition_customer_data or updating upon full duplicate
$stmt2 = mysqli_prepare($mysqlConn,
        "INSERT into ambition.ambition_customer_data(company_number, customer_number, first_name, last_name,
        phone, cell, home, work, email )
          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
          ON duplicate key update
             phone = values(phone),
             cell = values(cell),
             home = values(home),
             work = values(work),
             email = values(email)") or die(mysqli_error( $mysqlConn));

//Execute insert/update statement for customer data
mysqli_stmt_execute($stmt2) or die(mysqli_error($mysqlConn));


mysqli_close($mysqlConn);
odbc_close($DB2Conn);


?>
