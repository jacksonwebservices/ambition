<?php
// @author: Hunter Norman
// @last updated: 11/05/2018

/*For this script, the main focus is to create sectioned data between our phone database and our production database. The pupose of this is primarily to create current ambition data for our CSR agents. This script is to be run on an interval, ideally every 15 minutes. This will keep aggregated data for each rep throughout the day. The process is to select form joined tables on the phone database, insert these into our main production/ambition table, update the table with each users appropriate GoJFI extension ID based on their 4-digit extension, and then selecting the necessary data from that production ambition table.

Once this is all complete, the loop that stores the rows from that final select query will be creating an array that informs our final JSON file. This is what's uploaded daily to ambition.*/


$DB2Conn = odbc_connect("JFIWeblink","normanh", "Guitar138");

//Check DB2 Connection
if(!$DB2Conn){
  die("Could not connect");
}else{
echo "Connected Succssfully to DB2";
}


//query 1, this selects the joined data from all of the phone database tables and prepares it for insertion
$data = "
MERGE INTO jfidata.ambition_daily_call_totals AS d
USING
   (SELECT
      u.extension
      , u.user_id as extID
      , sum(duration) as total_talk_time_seconds
      , round(sum(duration) / 60,2) as total_talk_time_minutes
      , sum(case when legtype1 = 2 and answered = 1 then 1 else 0 end ) as total_inbound
      , sum(case when legtype1 = 1 then 1 else 0 end) as total_outbound
      , sum( case when(legtype1 = 1 and duration > 60) then 1 else 0 end) as credit_for_outbound
      , sum(case when legtype1 = 2 and answered = 0 then 1 else 0 end ) as total_missed
      , sum(case when legtype1 = 1 then 1 else 0 end) + sum(case when legtype1 = 2 then 1 else 0 end)  as total_calls
      , ROUND(COALESCE(100.00 * sum(case when legtype1 = 2 and answered = 1 then 1 end)/sum(case when legtype1 = 2 then 1 end), 100),2) as percent_answered
      , current_date as date_of_report
      , now() as time_of_report

    FROM jfidata.AMBITION_SESSION a
    JOIN jfidata.AMBITION_CALL_SUMMARY b
      ON a.notablecallid = b.notablecallid
    JOIN jfidata.AMBITION_USERS u
      ON u.EXTENSION = CALLINGPARTYNO or u.EXTENSION = FINALLYCALLEDPARTYNO
    WHERE u.active = 1
    and a.ts >= current_date
    GROUP BY extension, u.user_id
   ) AS q
ON (d.date_of_report = q.date_of_report
    AND d.extension = q.extension)
WHEN MATCHED THEN
     UPDATE SET d.total_talk_time_seconds = q.total_talk_time_seconds,
                d.total_talk_time_minutes = q.total_talk_time_minutes,
                d.total_inbound = q.total_inbound,
                d.total_outbound = q.total_outbound,
                d.credit_for_outbound = q.credit_for_outbound,
                d.missed_calls = q.total_missed,
                d.total_calls = q.total_calls,
                d.percent_answered = q.percent_answered
WHEN NOT MATCHED THEN
     INSERT (extension,
            ext_ID,
            total_talk_time_seconds,
            total_talk_time_minutes,
            total_inbound,
            total_outbound,
            credit_for_outbound,
            missed_calls,
            total_calls,
            percent_answered,
            date_of_report,
            time_of_report)
     VALUES (q.extension,
            q.extID,
            q.total_talk_time_seconds,
            q.total_talk_time_minutes,
            q.total_inbound,
            q.total_outbound,
            q.credit_for_outbound,
            q.total_missed,
            q.total_calls,
            q.percent_answered,
            q.date_of_report,
            q.time_of_report)
";

$prepData = odbc_prepare($DB2Conn, $data);
$executeData = odbc_execute($prepData);

if($executeData){
  echo "success";
}else{
  echo "failed" . odbc_errormsg();
}


$stmt3 = "
  MERGE INTO jfidata.ambition_daily_call_totals ct
  USING (
  SELECT
  COUNT (*) AS dealers,
  SUM( CASE WHEN f.follow_up_date BETWEEN CURRENT_DATE AND CURRENT_DATE + 7 DAYS THEN 1 ELSE 0 END ) AS contacts,
  CAST (ROUND((SUM( CASE WHEN f.follow_up_date BETWEEN CURRENT_DATE AND CURRENT_DATE + 7 DAYS THEN 1.0 ELSE 0 END )/ COUNT (*)) * 100.00, 2) AS DECIMAL (12, 2)) AS PERCENT,
  u.user_id as userID,
  u.first_name,
  u.last_name
  FROM jfidata.ambition_users u
  INNER JOIN jfidata.dealer_notet n
  ON n.user_identifier = u.user_id
  INNER JOIN jfidata.COMMUNICATION_METHODT m
  ON n.COMMUNICATION_METHODT_ID = m.COMMUNICATION_METHODT_ID
  LEFT JOIN jfidata.dealer_follow_up_date f
  ON f.dealer_number = n.dealer_number
  WHERE COMMUNICATION_CODE <> 'none'
  AND n.created_at >= CURRENT_DATE 
  GROUP BY u.user_id, u.first_name, u.last_name
  ) as cu
  on cu.userID = ct.ext_id and ct.date_of_report >= current_date
  WHEN MATCHED THEN UPDATE
  set ct.dealers_contacted = cu.dealers,
  ct.percent_up_to_date = cu.percent
";

$prepUpdate = odbc_prepare($DB2Conn, $stmt3);
$executeUpdate = odbc_execute($prepUpdate);

if($executeUpdate){
  echo "success";
}else{
  echo "failed" . odbc_errormsg();
}

/*New DB2 query*/
// UPDATE jfidata.ambition_daily_call_totals a
//     INNER JOIN
//     (SELECT
//          c.user AS UserID,
//          COUNT(*) AS dealers,
//          ROUND((al.NumberOfDealers / al.NumberOfDealerContacts) * 100 ,2)  AS percent
//      FROM connj.dealert_notet c
//      JOIN connj.follow_up_date d
//      ON c.dealer_number = d.dealer_number
//      LEFT JOIN (
//        SELECT user_id, COUNT(*) AS NumberOfDealerContacts,
//        SUM(CASE WHEN ( d.date + INTERVAL 7 DAY) THEN 1 ELSE 0 END) AS NumberOfDealers
//        FROM jfidata.ambition_users AS al
//        JOIN connj.custmstfc AS d ON d.CSR_CODE = al.attribute
//        GROUP BY user_id) AS al
//      ON al.user_id = c.user
//      WHERE c.created_at >= CURDATE()
//      AND c.communication_methodt_id <> 5
//      GROUP BY UserID) as cu
//     on cu.UserID = a.ext_id
//   SET a.dealers_contacted = cu.dealers and
//       a.percent_up_to_date = cu.percent
//   where a.date_of_report >= curdate()

// echo print_r($stmt);


// //This executes the update statement for ambition.ambition_daily_call_totals
// mysqli_stmt_execute($stmt3) or die(mysqli_error($DB2Conn));


// //Query 2, this selects needed data from the newly inserted/updated ambition.ambition_daily_call_totals table so that we can have the appropriately filled and formatted JSON file once all processes are finished
$selectForJSON = 
    'SELECT
          ext_id as "ext_id",
          extension as "extension",
          total_talk_time_seconds as "total_talk_time_seconds",
          total_talk_time_minutes as "total_talk_time_minutes",
          total_inbound as "total_inbound",
          total_outbound as "total_outbound",
          credit_for_outbound as "credit_for_outbound",
          missed_calls as "missed_calls",
          total_calls as "total_calls",
          percent_answered as "percent_answered",
          ifnull(dealers_contacted,0) as "dealers_contacted",
          ifnull(percent_up_to_date,0) as "percent_up_to_date",
          date_of_report as "date_of_report",
          time_of_report as "time_of_report"
    FROM jfidata.ambition_daily_call_totals
    WHERE Date_of_report >= CURRENT_DATE';


//new array specifically for the final JSON file
$content2 = array();

$result = odbc_exec($DB2Conn, $selectForJSON);

//creating array for new fetch since it now has the updated extension IDs
while ($d2 = odbc_fetch_array($result)) {


    //Store the current row
    $content2[] = $d2;
    echo $d2;

     }


// // Store it all into our final JSON file
file_put_contents('ambitionLog.json', json_encode($content2, JSON_PRETTY_PRINT ));


//Beginning code to upload to Ambition API via PDOStatement

$url = 'https://jacksonfurniture.ambition.com/api/v1/data/file_upload_972aa5df_b7dc_46df_b7ef_efa552269518_depot/';
$token = 'a54a24b8b4c4752fcb15c7500429c43023581c7f';

//Initiate CURL
$ch = curl_init($url);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_HTTPHEADER => array(
    'Authorization: Token '.$token,
    'Content-Type : application/json'
    ),
CURLOPT_POSTFIELDS => json_encode($content2, JSON_PRETTY_PRINT)
     ));

//Execute request
$postResult = curl_exec($ch);

// Check for errors
if($postResult === FALSE){
    die(curl_error($ch));
    }

// Decode the response
$responseData = json_decode($postResult, TRUE);


mysqli_close($DB2Conn);

?>
