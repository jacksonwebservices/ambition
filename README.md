# Ambition Documentation #
* Author: Hunter Norman
* Publishing Date: 1/3/2017

## Repository Scope/Overview ##
The files in this repository make up the ambition project which serves as a way to use incentivised metrics and scoring for our
CSRs, but will eventually expand to sales and potentially other areas of business. The bulk of data involved comes from the database
that is assigned to our phone system. Through a series of scripts running as scheduled jobs, we bring the data from the phone system 
into our own production database and then use queries to get our aggregated metrics based on each CSR.

The primary file used for obtaining these metrics, ambitiontestscript, will run every 15 minutes. This will preform several queries 
to calculate the metrics, insert into our ambition totals table (ambition.ambition_totals) and will then update the table based on the
users GoJFI User ID. At the end of this script, a JSON object/file (AmbitionLog.json) is created with these metrics applied in the 
necessary format for Ambition to accept on their end. Once the JSON has been created, we use a cURL instance to upload into our data 
integration using Ambition's API. 

These jobs are currently running on our dev server 
> 192.168.21.234


## Script Dependencies ##

The necessary requirements for these scripts to run are:
* PHP
* MySQL (Phone Database: 192.168.20.13 using cdr_reader as user and pass, Production database: 192.168.20.11 )
* Internet Access and cURL ability to reach out to Ambition's API

## Script Sequence and Definitions ##
The scripts are running in a calculated sequence to ensure the data is as up to date and accurate as possible. The current sequence
of the scripts is as follows:

###Every 10 Minutes###
* updatePhoneTables.php (This script simply updates the phone system tables in our schema(20.11) with the newest data from the phone schema (20.13))
* updateCustomerData.php (This will connect to DB2 and select from our customer master file and sales phonebook to update ambition.ambition_customer_data with any new phone info) 
###Every 15 Minutes###
* ambitionTestScript.php (This is the main script that gets our aggregated totals)

Since we didn't have the luxury of setting up our own phone system or at least a data instance on our own server, the scripts are giving
us a means to take the data we need and formulate the metrics necessary to give to Ambition. There are better ways to do what is currently 
being done but some of that depends on whether or not we implement a new phone system, which has been discussed. In that event, we can 
streamline a lot of this work at that time and cut out some extra steps.

## MySQL Schemas/Tables  ##
###Schemas###
* Ambition (192.168.20.11)
* CDRDB (192.168.20.13)

As far as phone data is concerned, I have cloned the structure of our phone system database (192.168.20.13) and put those tables into our own Ambition 
schema on the PROD server (192.168.20.11). When the updatePhoneTables.php script runs, we're basically doing an INSERT IGNORE in order to get any records 
from the source database that don't already exist in our database. This way, we only have this one process that touches the phone system database in order 
to update records, and we can query for the aggregated metrics on our own server, minimizing the amount of connections during that process. 

It's worth noting that several of the tables in the phone system database have either never been setup/used and are blank or have a small number of test records. Some
tables also appear to rarely be used and their purpose is unknown. They have been cloned and are included in the update for the sake of awareness and completion. 

Also, in terms of the data needed for ambition and metrics in general, the only tables regarding phone data that are of true concern are: 
    callsummary and session

The phone system was not set up by our department, and there is little to no knowledge of exactly how or why certain things were implemented. Tables names, data types,
etc. were established at the time of setup, we are simply inheriting that structure on our schema, as well as adding the ambition tables. I have only included the tables
that are used/populated on CDRDB in the updatePhoneTables.php script.

###Tables###
 The following tables exist both on the CDRDB Schema (Original records, 192.168.20.13) and the Ambition Schema (Cloned tables, 20.11):

Both Schemas  | Ambition Schema Only
------------- | --------------------
aainput | ambition_customer_data
acdgroup | ambition_totals
acdqueue | ambition_users
agentgrouppresence | known_numbers
agentlogin |
agentpresnce | 
cadvalue |
callbackattempt |
callbackrequest |
callnote |
callsummary |
customuserfield |
customuserfieldname |
device |
discretedates | 
mxuser |
sessfilter | 
session |
upgrade |
userpresence |
userpresencebackup |
version |
voicemail |

The bulk of this project exists in the three scripts running to keep our own server/schema up to date with this phone/call record data and then inserting the metrics 
into our ambition_totals table. After the tables are updated and the final script has run, the work transfers to the ambition side. 

## Ambition Site Overview ##
###NOTE: In the event of fild upload failure, go [here](#markdown-header-file-upload)###
* [here](#markdown-header-failure)
Our Ambition account is located at: https://jacksonfurniture.ambition.com

###Admin Credentials###
First Name: Jackson
Last Name: Admin
Email: webadmin@jacksonfurnind.com
Password: aiJ#83KDj*kaj+U

###API Credentials###
Endpoint: https://jacksonfurniture.ambition.com/api/v1/data/file_upload_972aa5df_b7dc_46df_b7ef_efa552269518_depot/
Auth Token: a54a24b8b4c4752fcb15c7500429c43023581c7f

###Data Integration###
On the left sidebar of the site, go to:
    Administration > Data > Integrations
On the integration page, click on 'settings' for the 'JFI Upload in JSON' integration. This is our primary data integration.

Once at the settings page, there are multiple options, many of which we will rarely if ever need to use. However, in the event
of upload failure or the need to add/remove metrics, this page is crucial.

####Status####
This page simply shows the current status of our file upload depot, including a reason code for the current status (if applicable)
and a timestamp for the last write/upload.

####Setup####
#####Settings#####
Pretty basic settings for contact, notifications and sandbox mode. This is where you can add more transport/upload methods and also clone
or deactivate the integration.

#####Credentials#####
Here you will find our auth token, route and endpoint for the api. This should never be edited unless instructed.

#####Data Format#####
Our current file format is JSON and should remain so. We can also change the data format type for records and metrics. Ambition has set up
a strict way of applying data format to record values and file uploads hinge on this format completely. Similar to a database, this uses 
unique record identifiers and IDs, as well as data types for metrics, sums and percentages. This page gives the option to change the type
or display name for any field key in our records. More on this in the documentation for the data format page. 

####Metric Builder####
#####Add#####
Very basic setup to add the name and description of a new metric. Once established, you must choose the data integration from the 'Object' dropdown,
in our case  it would be the 'JFI Upload in JSON' object.

#####Manage#####
Shows all current metrics with their attached object and last update time, along with options to edit, delete or add new.

####Data####
#####Uploads#####
Shows  a list of all file uploads with the ability to view each one, showing the raw JSON object. This shows whether each file was processed, the 
timestamp, upload type, # of records generated from the upload, size and errors.

#####Records#####
A history of records for all users, which can be expanded to show user detail. This is a detailed view of records created from the previous upload 
section. 

#####Metrics#####
Since uploaded records can be different from the metrics themselves, this screen shows a detailed histroy of metrics for each user, such as 'Percent
Answered'. This page can be filtered by time group (such as 'Day'), User, Date, and Metric.

#####File-Upload#####
This page gives us the ability to manually upload files, which was the method we had to use before the API was set up. This is very critical in the event 
of upload failure from the script that runs on our dev server (20.234). For instance, if the server is down or cannot access the Ambition API. If the script
was run and created the JSON object, but faile to upload, then we can navigate to this page (https://jacksonfurniture.ambition.com/integration-management/file-upload/12/)
and we will be able to select 'Choose File' and navigate to our root folder for our AmbitionLog.JSON file that was just created.

In the event the script didn't run at all, we will have to run ambitionTestScript.php locally, and use the ambitionLog.JSON file created from that instead. 

####Tools####
#####API Sandbox#####
This is rarely used but if we need to put our API integration into sandbox mode, we can visit this to view the API details

Also, there are a few other important options under the 'Administration' menu option, such as:

* Organization (details about our organization an d its preformance within Ambition)
* People (the ability to add users, add/edit groups and modify permissions)
* Scorecards, goals and competition (The ability to edit and edd preformance goals, create customized scorecards and make new competitions for groups and specific users)
* TVs (since our users are viewing all of the metric data on TVs at the corporate office, this is needed to create 'Slides' that play on each TV. Each TV has its own instance and slides, which can be edited at any time.)

Full ambition documentation is available at: https://help.ambition.com/
The overall FAQ is at: https://help.ambition.com/docs/faq-general
Also, by clicking the 'Get Help' tool tip at the bottom of the page, we have the ability to chat with an associate or submit a ticket.

## Who do I talk to? ##

For issues with Ambition's site or data, visit: https://help.ambition.com/ or email Travis at travis.truett@ambition.com

For questions regarding the creation of this README, Repo or codebase contact me via email or slack.
