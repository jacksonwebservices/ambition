<?php
// @author: Hunter Norman
// @last updated: 2/27/2019


$DB2conn = odbc_connect("JFIWeblink","normanh", "Guitar138");

//Check DB2 Connection
if(!$DB2conn){
  die("Could not connect");
}else{
echo "Connected Succssfully to DB2 PROD";
}


$buildMerge = "
	MERGE INTO JFIDATA.ambition_call_activity AS d
	USING (
			select
        tm.parent_territory as TERRITORY,
        sum(case when a.actcode = 'STOREVISIT' then 1 else 0 end) as DAILY_STORE_VISIT,
        sum(case when a.actcode = 'SALESMEET' then 1 else 0 end) as DAILY_SALES_MEET,
        sum(case when a.actcode = 'OFFICE' then 1 else 0 end) as DAILY_OFFICE,
        sum(case when a.actcode = 'NEWACCT' then 1 else 0 end) as NEW_ACCT,
        sum(case when a.actcode = 'OTHER' then 1 else 0 end) as OTHER,
        current_date as date_of_report
      from  jfidata.territory_mappings tm
      left join patm.cractrpt a
        on tm.child_territory = a.slsno
      Where a.entdate >= CURRENT_DATE
      group by tm.parent_territory
    	) AS q 
    ON (d.date_of_report = q.date_of_report AND d.territory = q.territory)
    WHEN MATCHED THEN
    	UPDATE SET  d.DAILY_STORE_VISIT = q.DAILY_STORE_VISIT,
                  d.DAILY_SALES_MEET = q.DAILY_SALES_MEET,
                  d.DAILY_OFFICE = q.DAILY_OFFICE,
                  d.NEW_ACCT = q.NEW_ACCT,
                  d.OTHER = q.OTHER

    WHEN NOT MATCHED THEN
      INSERT (TERRITORY,
              DAILY_STORE_VISIT,
              DAILY_SALES_MEET,
              DAILY_OFFICE,
              NEW_ACCT,
              OTHER,
              date_of_report)
		  VALUES (q.TERRITORY,
            q.DAILY_STORE_VISIT,
            q.DAILY_SALES_MEET,
            q.DAILY_OFFICE,
            q.NEW_ACCT,
            q.OTHER,
            q.date_of_report)
";

$prepBuild = odbc_prepare($DB2conn, $buildMerge);
$executeBuild = odbc_execute($prepBuild);

if($executeBuild){
	echo "Successfully Merged";
}else{
	echo "failed" . odbc_errormsg();
}



$selectForJSON = 
    "
    SELECT
      TERRITORY,
      DAILY_STORE_VISIT,
      DAILY_SALES_MEET,
      DAILY_OFFICE,
      NEW_ACCT,
      OTHER,
      DATE_OF_REPORT
    from jfidata.ambition_call_activity
    where date_of_report >= current_date
    ";


//new array specifically for the final JSON file
$content2 = array();

$result = odbc_exec($DB2conn, $selectForJSON);

//creating array for new fetch since it now has the updated extension IDs
while ($d2 = odbc_fetch_array($result)) {


    //Store the current row
    $content2[] = $d2;
    echo $d2;

     }


// // Store it all into our final JSON file
file_put_contents('salesCallActivityLog.json', json_encode($content2, JSON_PRETTY_PRINT ));


//Beginning code to upload to Ambition API via PDOStatement

$url = 'https://jacksonfurniture.ambition.com/api/v1/data/file_upload_9eee2b27_a333_408b_9417_01df6b8c8b68_depot/';
$token = 'dd7c1866a55820223f7a1896ff14b5084e46d625';

//Initiate CURL
$ch = curl_init($url);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_HTTPHEADER => array(
    'Authorization: Token '.$token,
    'Content-Type : application/json'
    ),
CURLOPT_POSTFIELDS => json_encode($content2, JSON_PRETTY_PRINT)
     ));

//Execute request
$postResult = curl_exec($ch);

// Check for errors
if($postResult === FALSE){
    die(curl_error($ch));
    }

// Decode the response
$responseData = json_decode($postResult, TRUE);

$DB2conn = null;

?>