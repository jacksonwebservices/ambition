<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/vendor/autoload.php';

print_r(get_included_files());

// @author: Hunter Norman
// @last updated: 09/18/2017


/*
This script will run once at the end of the week, and is the final in a sequence of php scripts related to our phone database and ambition data. Once all tables in our database have been updated and all numbers have been aggregated/totaled, the script will generate a weekly report with aggregated totals for the week per CSR and will include scores. The script will insert into a table for  historical purposes but the report will be saved per day and mailed to Todd Cherry/Web Admin.
*/


//Defining credentials
// $servername = "192.168.20.13";
// $username = "cdr_reader";
// $password = "cdr_reader";

$servername2 = "192.168.20.11";
$username2 = "normanh";
$password2 = "Guitar138*";

// Create connection
//$conn = new mysqli($servername, $username, $password);
$conn2 = new mysqli($servername2, $username2, $password2);

// Check connection
// if ($conn->connect_error) {
//     die("Connection failed: " . $conn->connect_error);
// }
// echo "Connected successfully";

// Check connection2
if ($conn2->connect_error) {
    die("Connection failed: " . $conn2->connect_error);
}
echo "Connected successfully";


/*REPORTS BEGIN BELOW*/

$result = $conn2->query(
	"SELECT
    		firstn ,
    		lastn ,
    		extension ,
    		Total_Outbound+Total_Missed+Total_Received AS Total_Calls ,
    		Total_Known,
    		Total_Received,
    		recieved_known,
    		Total_Outbound,
    		outbound_known,
    		Total_Missed ,
    		missed_known,
    		Total_Talk_Time_minutes,
    		round(Total_Talk_Time_minutes/5, 2) AS Average_Talk_Time_Daily,
    		round(Total_Talk_Time_minutes/(total_received + total_outbound), 2) AS Average_Talk_Time_Per_call,
    		round(Total_Missed  / (Total_Outbound+Total_Missed+Total_Received)  * 100, 2) AS Percentage_Missed,
    		round(missed_known / total_known * 100,2) AS Missed_percent_known
     FROM (
     SELECT
  	    u.firstn,
  	    u.lastn,
  	    c.extension,
  	    sum(CASE WHEN CALLINGPARTYNO = k.phone_number THEN 1
  				 WHEN FINALLYCALLEDPARTYNO = k.phone_number THEN 1 ELSE 0 END ) AS total_known,
  	    sum(if(Answered = 1,0,1)) AS Total_Missed ,
  	    sum(CASE WHEN Answered = 0 AND CALLINGPARTYNO = k.phone_number THEN 1 ELSE 0 END ) AS missed_known,
  	    sum(CASE WHEN LEGTYPE1 = 2 AND ANSWERED = 1 THEN 1 ELSE 0 END) AS Total_Received ,
  	    sum(CASE WHEN LEGTYPE1 = 2 AND answered = 1 AND CALLINGPARTYNO = k.phone_number THEN 1 ELSE 0 END) AS recieved_known,
  	    sum(CASE WHEN LEGTYPE1 = 1 THEN 1 ELSE 0 END) AS Total_Outbound ,
  	    sum(CASE WHEN LEGTYPE1 = 1 AND FINALLYCALLEDPARTYNO = k.phone_number THEN 1 ELSE 0 END) AS outbound_known,
  	    round(sum(Duration) / 60,2) AS Total_Talk_Time_minutes

  	  FROM ambition.session a
  	  INNER JOIN ambition.callsummary b ON a.NOTABLECALLID = b.NOTABLECALLID
  	  INNER JOIN ambition.mxuser c ON a.RESPONSIBLEUSEREXTENSIONID = c.EXTENSIONID
  	  INNER JOIN jackson_id.users u ON c.extension = u.extension
  	  LEFT JOIN ambition.known_numbers k ON a.callingpartyno = k.phone_number
  	  WHERE b.ts BETWEEN curdate() - interval 5 day AND now()
  	  AND c.extension IN (7276,7314,7295,7306,7357,7200,7218,7247,7331,7255,7330,7000,7215,7240,7358,7312)
  	  GROUP BY c.extension, u.firstn, u.lastn ) x")  or die(mysqli_error( $conn2));


if (!$result) die('Couldn\'t fetch records');
$num_fields = mysqli_num_fields($result);
$headers = array();
while ($fieldinfo = mysqli_fetch_field($result)) {
    $headers[] = $fieldinfo->name;
	}
$fp = fopen('weeklyReportTestPHP.csv', 'w');
if ($fp && $result) {
    fputcsv($fp, $headers);
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        fputcsv($fp, array_values($row));
    }
    fclose($fp);
}


//Add Mailto
$file = "weeklyReportTestPHP.csv";

$mail = new PHPMailer(true);
$address ="hnorman@jacksonfurnind.com";
$date = date("D M d, Y");


try{
$mail->setFrom("hnorman@jacksonfurnind.com");
$mail->addAddress($address);
$mail->AddAttachment($file, "WeeklyReport$date.csv");
$mail->isHTML(true);
$mail->Subject    = "Weekly CSR Report";
$mail->Body       = "Attached is the weekly report for " . $date;
$mail->Send();
echo 'message sent';

} catch (Exception $e){
	echo 'message failed';
	echo 'mail error:' . $mail->ErrorInfo;
}

//mysqli_close($conn);
mysqli_close($conn2);
?>
