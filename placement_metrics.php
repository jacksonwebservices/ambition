<?php
// @author: Hunter Norman
// @last updated: 2/27/2019


$DB2conn = odbc_connect("JFIWeblink","normanh", "Guitar138");

//Check DB2 Connection
if(!$DB2conn){
  die("Could not connect");
}else{
echo "Connected Succssfully to DB2 PROD";
}

$buildMerge = "
	MERGE INTO JFIDATA.AMBITION_ACTIVE_PLACEMENT_METRIC AS d
	USING(
		SELECT
		  tm.parent_territory as TERRITORY,
		  tm.name as NAME,
		  SUM(CASE WHEN current_date between p.start_date and p.expire_date then 1 else 0 end) AS ACTIVE_PLACEMENTS,
		  CURRENT_DATE AS DATE_OF_REPORT
		FROM JFIDATA.TERRITORY_MAPPINGS tm
		  inner join jfidata.placement_list p
		    on tm.child_territory = p.SALES_REP
		group by parent_territory, name
    	) AS q 
    ON (d.date_of_report = q.date_of_report AND d.TERRITORY = q.TERRITORY)
    WHEN MATCHED THEN
    	UPDATE SET  d.ACTIVE_PLACEMENTS = q.ACTIVE_PLACEMENTS
    WHEN NOT MATCHED THEN
    	INSERT (TERRITORY,
    			NAME,
    			ACTIVE_PLACEMENTS,
    			DATE_OF_REPORT)
		VALUES (q.TERRITORY,
    			q.NAME,
    			q.ACTIVE_PLACEMENTS,
    			q.DATE_OF_REPORT)
";

$prepBuild = odbc_prepare($DB2conn, $buildMerge);
$executeBuild = odbc_execute($prepBuild);

if($executeBuild){
	echo "Successfully Merged";
}else{
	echo "failed" . odbc_errormsg();
}
    



$selectForJSON = 
    'SELECT territory,ACTIVE_PLACEMENTS,date_of_report FROM JFIDATA.AMBITION_ACTIVE_PLACEMENT_METRIC where date_of_report >= current_date';


//new array specifically for the final JSON file
$content2 = array();

$result = odbc_exec($DB2conn, $selectForJSON);

//creating array for new fetch since it now has the updated extension IDs
while ($d2 = odbc_fetch_array($result)) {


    //Store the current row
    $content2[] = $d2;
    echo $d2;

     }


// // Store it all into our final JSON file
file_put_contents('salesPlacementLog.json', json_encode($content2, JSON_PRETTY_PRINT ));


//Beginning code to upload to Ambition API via PDOStatement

$url = 'https://jacksonfurniture.ambition.com/api/v1/data/file_upload_e60bdf75_c639_428d_9b2b_2efd86907bc6_depot/';
$token = '03fa6b3ad7a38ddf1ca4f4567bec1ae91660207f';

//Initiate CURL
$ch = curl_init($url);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_HTTPHEADER => array(
    'Authorization: Token '.$token,
    'Content-Type : application/json'
    ),
CURLOPT_POSTFIELDS => json_encode($content2, JSON_PRETTY_PRINT)
     ));

//Execute request
$postResult = curl_exec($ch);

// Check for errors
if($postResult === FALSE){
    die(curl_error($ch));
    }

// Decode the response
$responseData = json_decode($postResult, TRUE);

$DB2conn = null;
?>