<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/vendor/autoload.php';

print_r(get_included_files());

// @author: Hunter Norman
// @last updated: 09/18/2017


/*
This script will run last in a sequence of daily php scripts related to our phone database and ambition data. Once all tables in our database have been updated and all numbers have been aggregated/totaled, the script will generate a daily report with CSR numbers per call. The script will insert into a table for  historical purposes but the report will be saved per day and mailed to Todd Cherry/Web Admin.
*/


//Defining credentials
$servername = "192.168.20.13";
$username = "cdr_reader";
$password = "cdr_reader";

$servername2 = "192.168.20.11";
$username2 = "normanh";
$password2 = "Guitar138*";

// Create connection
$conn = new mysqli($servername, $username, $password);
$conn2 = new mysqli($servername2, $username2, $password2);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

// Check connection2
if ($conn2->connect_error) {
    die("Connection failed: " . $conn2->connect_error);
}
echo "Connected successfully";


/*REPORTS BEGIN BELOW*/

$result = mysqli_query($conn2,
"SELECT
	  FirstN
	  , LastN
    , Extension
    , Recieved
    , Recieved_Known
    , Outbound
    , Outbound_Known
    , Missed_No_VM
    , Missed_VM
    , Missed_Known
    , Calling_Number
    , Called_Number
    , Start_Time
    , End_Time
	, Talk_Time_Seconds
    , Hold_Time_Seconds

	FROM (
      SELECT distinct
       firstn
       , lastn
       , c.extension
       , CASE WHEN LEGTYPE1 = 2 AND ANSWERED = 1 THEN 'x' ELSE '' END AS Recieved
       , case when LEGTYPE1 = 2 and answered = 1 and CALLINGPARTYNO = k.phone_number then 'x' ELSE '' end as Recieved_Known
       , CASE WHEN LEGTYPE1 = 1 then 'x' ELSE '' end  AS Outbound
       , case when  FINALLYCALLEDPARTYNO = kn.long_number then 'x' ELSE '' end as Outbound_Known
       , case when legtype1 = 2 and answered = 0 and finallycalledpartyno  not like '%oice%' then 'x' ELSE '' end as Missed_No_VM
       , case when finallycalledpartyno like '%oice%' then 'x' ELSE '' end as Missed_VM
       , case when legtype1 = 2 and ANSWERED = 0 and CALLINGPARTYNO = k.phone_number then 'x' ELSE '' end as
        Missed_Known
       , a.CALLINGPARTYNO AS Calling_Number
       , a.FINALLYCALLEDPARTYNO AS Called_Number
       , b.starttime as Start_Time
       , b.endtime as End_Time
       , b.duration as Talk_Time_Seconds
       , a.holdtimesecs as Hold_Time_Seconds

      FROM ambition.session a
		    INNER JOIN ambition.callsummary b ON a.NOTABLECALLID = b.NOTABLECALLID
		    right join jackson_id.users c on a.callingpartyno = c.extension or a.finallycalledpartyno = c.extension
		    LEFT JOIN ambition.known_numbers k ON a.callingpartyno = k.phone_number
        left join ambition.known_numbers kn on a.finallycalledpartyno = kn.long_number
			    WHERE a.ts >= curdate()
			    and(a.CALLINGPARTYNO in (select extension from ambition.ambition_users) OR a.finallycalledpartyno IN (select extension from ambition.ambition_users))
      ) x
      order by extension;") or die(mysqli_error( $conn2));



$result2 = mysqli_query($conn2,
"SELECT
    FirstN
    , LastN
    , Extension
    , Total_Recieved
    , Total_Known_RCVD
    , Total_Outbound
    , Total_known_OTBND
    , Total_Missed
    , Total_Known_MSSD
    , (Total_Recieved + Total_Outbound + Total_Missed) AS Total_Calls
    , Total_Talk_Time_Minutes
    , Total_Hold_Minutes

  FROM (
      SELECT distinct
            firstn
          , lastn
          , c.extension
          , sum(CASE WHEN LEGTYPE1 = 2 AND ANSWERED = 1 THEN 1 ELSE 0 END) AS Total_Recieved
          , sum(case when LEGTYPE1 = 2 and answered = 1 and CALLINGPARTYNO = k.phone_number then 1 ELSE 0 end) as Total_Known_RCVD
          , sum(CASE WHEN LEGTYPE1 = 1 then 1 ELSE 0 end)  AS Total_Outbound
          , sum(case when  FINALLYCALLEDPARTYNO = kn.long_number then 1 ELSE 0 end) as Total_known_OTBND
          , sum(case when Legtype1 = 2 and Answered = 0 then 1 ELSE 0 end) as Total_Missed
          , sum(case when ANSWERED = 0 and CALLINGPARTYNO = k.phone_number then 1 ELSE 0 end)   as Total_Known_MSSD
          , sum(CASE WHEN LEGTYPE1 = 1 OR LEGTYPE1 = 2 THEN 1 ELSE 0 END) as Total_Calls
          , Round(sum(b.duration) /60,2) as Total_Talk_Time_Minutes
          , Round(sum(a.holdtimesecs) /60,2) as Total_Hold_Minutes
      FROM jackson_id.users c
          INNER JOIN ambition.session a  on c.extension = a.callingpartyno or c.extension = a.finallycalledpartyno
          INNER JOIN ambition.callsummary b ON a.NOTABLECALLID = b.NOTABLECALLID
          LEFT JOIN ambition.known_numbers k ON a.callingpartyno = k.phone_number
          left join ambition.known_numbers kn on a.finallycalledpartyno = kn.long_number
              WHERE a.ts >= curdate()
                  AND(a.CALLINGPARTYNO in (select extension from ambition.ambition_users)
                  OR a.finallycalledpartyno IN (select extension from ambition.ambition_users)
      )
      GROUP BY
            firstn
          , lastn
          , c.extension
    ) x
    ORDER BY lastn
    ;") or die(mysqli_error( $conn2));



if (!$result) die('Couldn\'t fetch records');
$num_fields = mysqli_num_fields($result);
$headers = array();
while ($fieldinfo = mysqli_fetch_field($result)) {
    $headers[] = $fieldinfo->name;
}
$fp = fopen('dailyReportTestPHP.csv', 'w');
if ($fp && $result) {
    fputcsv($fp, $headers);
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        fputcsv($fp, array_values($row));
    }
		fclose($fp);
}

if (!$result2) die('Couldn\'t fetch records');
$num_fields = mysqli_num_fields($result2);
$headers = array();
while ($fieldinfo = mysqli_fetch_field($result2)) {
    $headers[] = $fieldinfo->name;
}
$fp = fopen('dailyReportTestTotals.csv', 'w');
if ($fp && $result2) {
    fputcsv($fp, $headers);
    while ($row = $result2->fetch_array(MYSQLI_NUM)) {
        fputcsv($fp, array_values($row));
    }
		fclose($fp);
}

$file = "dailyReportTestPHP.csv";
$file2 = "dailyReportTestTotals.csv";

$mail = new PHPMailer(true);
 $address = "smedlin@jacksonfurnind.com";
//$address = "hnorman@jacksonfurnind.com";

$date = date("D M d, Y");


try{
$mail->setFrom("hnorman@jacksonfurnind.com");
$mail->addAddress($address);
$mail->addAddress('tcherry@jacksonfurnind.com', 'Todd Cherry');
$mail->AddAttachment($file,"Daily$date.csv");
$mail->AddAttachment($file2,"DailyTotals$date.csv");
$mail->isHTML(true);
$mail->Subject    = "Daily CSR Report";
$mail->Body       = "Attached is the daily CSR report for " . $date;
$mail->Send();
echo 'message sent';

} catch (Exception $e){
	echo 'message failed';
	echo 'mail error:' . $mail->ErrorInfo;
}

mysqli_close($conn);
mysqli_close($conn2);
?>
