<?php

/*
The purpose of this script is to run in sequence with a number of other php scripts that update information on our database server with real-time information that is stored on the phone system database server. This script will run first in the sequence to ensure that all call session tables are up to date with the latest call data before any reports are run and before aggregated csr totals are inserted into ambition tables.

Our database server has an exact clone of the phone database, so this script will check through all tables (even though only 4 are used by us directly at any given time) on the phone database and update our cloned tables with any new records to ensure that everything stays up to date and synced.

It's important to note that some of these tables on the phone system server are empty, and have been, seeming to simply be placeholder tables. It's unknown if they are intended to ever be inserted into, but this script will insure all tables are up to date on our production server regardless

These queries each explicitly name the columns  and values, which at times can be up to 59 columns in a table. This is really the case for session and callsummary. While it seems extraneous, the reason is to keep these queries future proof and allow us to pinpoint any errors that may ever come up from changes in the phone database on the ''''20.13 server. Especially since these tables are mainly untouched by developers and their relations/contents are widely unknown, I felt it best to keep the queries themselves rather explicit to allow for transparency in future changes.
*/

// @author: Hunter Norman
// @last updated: 11/01/2018


//In the event that there is a larger number of rows to update
ini_set('memory_limit', '256M');

//Defining credentials for phone server
$servername = "192.168.20.13";
$username = "cdr_reader";
$password = "cdr_reader";

// Create connections
$conn = new mysqli($servername, $username, $password);
//$conn2 = new mysqli($servername2, $username2, $password2);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

$DB2Conn = odbc_connect("JFIWeblink","normanh", "Guitar138");

//Check DB2 Connection
if(!$DB2Conn){
  die("Could not connect");
}
echo "Connected Succssfully to DB2";

/********************************************************************/
/****SCRIPT TO UPDATE JFIDATA.AMBITION_SESSION FROM CDRDB.SESSION****/
/********************************************************************/

$latest_result = "SELECT MAX(SESSIONID) as SESSIONID FROM jfidata.ambition_session";
$result = odbc_exec($DB2Conn, $latest_result);
$rows = odbc_fetch_object($result); 
echo $rows->SESSIONID;
$maxResult = $rows->SESSIONID;

//Select All rows from the source phone database
$source_data = mysqli_query($conn,"
  SELECT 
    SESSIONID
    ,CALLINGPARTYNO
    ,FINALLYCALLEDPARTYNO
    ,HOLDTIMESECS
    ,LEGTYPE1
    ,LEGTYPE2
    ,EXTENSIONID1
    ,EXTENSIONID2
    ,NOTABLECALLID
    ,RESPONSIBLEUSEREXTENSIONID
    ,TS 
  FROM `cdrdb`.`session` 
  WHERE `SESSIONID` > $maxResult");

// Loop on the results
while($source = $source_data->fetch_assoc()) {

  // Check if row exists in destination phone database
  $checkRow = "SELECT SESSIONID FROM jfidata.AMBITION_SESSION WHERE SESSIONID = '".$source['SESSIONID']."'";
  $prepareRowCheck = odbc_prepare($DB2Conn, $checkRow);
  $row_exists = odbc_execute($prepareRowCheck);
  //$row_exists = $DB2Conn->query("SELECT SESSIONID FROM jfidata.ambition_session WHERE SESSIONID = '".$source['SESSIONID']."' ") or die(db2_conn_error($DB2Conn));

  //if query returns false, rows don't exist with that new ID.
  if ($row_exists->num_rows == 0){
    echo "Yes";

    //Insert new rows into ambition.session
    $stmt = "
      INSERT INTO jfidata.AMBITION_SESSION (SESSIONID,CALLINGPARTYNO,FINALLYCALLEDPARTYNO,HOLDTIMESECS,LEGTYPE1,LEGTYPE2,EXTENSIONID1,EXTENSIONID2,NOTABLECALLID,RESPONSIBLEUSEREXTENSIONID,TS)
      VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
      or die(db2_conn_error($DB2Conn)) ;

    $prepInsert = odbc_prepare($DB2Conn, $stmt);

    $values = [
    $source['SESSIONID']
    ,$source['CALLINGPARTYNO']
    ,$source['FINALLYCALLEDPARTYNO']
    ,$source['HOLDTIMESECS']
    ,$source['LEGTYPE1']
    ,$source['LEGTYPE2']
    ,$source['EXTENSIONID1']
    ,$source['EXTENSIONID2']
    ,$source['NOTABLECALLID']
    ,$source['RESPONSIBLEUSEREXTENSIONID']
    ,$source['TS']];

    if($prepInsert){
      $result = odbc_execute($prepInsert,$values);
      if($result){
        print "successfully added record";
      }
    }
  }
}


/*****************************************************************************/
/****SCRIPT TO UPDATE JFIDATA.AMBITION_AMBITION_CALL_SUMMARY FROM CDRDB.CALLSUMMARY****/
/*****************************************************************************/

$latest_result_CALLSUMMARY = "SELECT MAX(NOTABLECALLID) as NOTABLECALLID FROM jfidata.AMBITION_CALL_SUMMARY";
$result_CALLSUMMARY = odbc_exec($DB2Conn, $latest_result_CALLSUMMARY);
$CALLSUMMARY_ROWS = odbc_fetch_object($result_CALLSUMMARY); 
echo $CALLSUMMARY_ROWS->NOTABLECALLID;
$maxResult_CALLSUMMARY = $CALLSUMMARY_ROWS->NOTABLECALLID;

//Select All rows from the source phone database
$source_data_CALLSUMMARY = mysqli_query($conn,"
  SELECT 
    NOTABLECALLID
    ,STARTTIME
    ,ENDTIME
    ,DURATION
    ,ANSWERED
    ,TS
    ,FIRSTCALLID
  FROM `cdrdb`.`callsummary` 
  WHERE `NOTABLECALLID` > $maxResult_CALLSUMMARY");

// Loop on the results
while($source_CALLSUMMARY = $source_data_CALLSUMMARY->fetch_assoc()) {
  echo "successful select";

  // Check if row exists in destination phone database
  $checkRow_CALLSUMMARY = "SELECT NOTABLECALLID FROM jfidata.AMBITION_CALL_SUMMARY WHERE NOTABLECALLID = '".$source_CALLSUMMARY['NOTABLECALLID']."'";
  $prepareRowCheck_CALLSUMMARY = odbc_prepare($DB2Conn, $checkRow_CALLSUMMARY);
  $row_exists_CALLSUMMARY = odbc_execute($prepareRowCheck_CALLSUMMARY);
  //$row_exists_CALLSUMMARY = $DB2Conn->query("SELECT NOTABLECALLID FROM jfidata.ambition_session WHERE NOTABLECALLID = '".$source_AMBITION_CALL_SUMMARY['NOTABLECALLID']."' ") or die(db2_conn_error($DB2Conn));

  //if query returns false, rows don't exist with that new ID.
  if ($row_exists_CALLSUMMARY->num_rows == 0){
    echo "Yes";

    //Insert new rows into ambition.session
    $STMT_CALLSUMMARY = "
      INSERT INTO jfidata.AMBITION_CALL_SUMMARY (NOTABLECALLID,STARTTIME,ENDTIME,DURATION,ANSWERED,TS,FIRSTCALLID)
      VALUES ( ?, ?, ?, ?, ?, ?, ?)"
      or die(db2_conn_error($DB2Conn)) ;

    $prepInsert_CALLSUMMARY = odbc_prepare($DB2Conn, $STMT_CALLSUMMARY);

    $values_CALLSUMMARY = [
    $source_CALLSUMMARY['NOTABLECALLID']
    ,$source_CALLSUMMARY['STARTTIME']
    ,$source_CALLSUMMARY['ENDTIME']
    ,$source_CALLSUMMARY['DURATION']
    ,$source_CALLSUMMARY['ANSWERED']
    ,$source_CALLSUMMARY['TS']
    ,$source_CALLSUMMARY['FIRSTCALLID']];

    if($prepInsert_CALLSUMMARY){
      $result_CALLSUMMARY = odbc_execute($prepInsert_CALLSUMMARY,$values_CALLSUMMARY);
      if($result_CALLSUMMARY){
        print "successfully added record";
      }
    }
  }
}
       
mysqli_close($conn);
odbc_close($DB2Conn);
?>
