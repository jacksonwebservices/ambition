<?php
// @author: Hunter Norman
// @last updated: 2/27/2019


$DB2conn = odbc_connect("JFIWeblinkProd","normanh", "Guitar138");
$DB2dev = odbc_connect("JFIWeblinkDev","normanh", "Guitar138");

//Check DB2 Connection
if(!$DB2conn){
  die("Could not connect");
}else{
echo "Connected Succssfully to DB2 PROD";
}

if(!$DB2dev){
  die("Could not connect");
}else{
echo "Connected Succssfully to DB2 DEV";
}

$buildMerge = "
	MERGE INTO JFIDATA.AMBITION_YELLOWBAR_METRICS AS d
	USING(
		SELECT distinct
			tm.parent_territory as Territory,
			tm.name as Rep_Name,
			tm.child_territory as SubTerritory,
			qw.prdcat ,
	  		(select cast(COALESCE(sum(slprcc),0) as integer)
	    		from jfidata.gporpcflt g
	   			where DATE(INSERT(INSERT(LEFT(CHAR(g.extd1d),8),5,0,'-'),8,0,'-')) >= CURRENT_DATE
	   			and qw.prdcat = g.extr5d
	   			and tm.child_territory = g.slsnoc) as dailyTotal,
			CAST(qq.qtayr/52 as integer) as weekQuota,
	  		CURRENT_DATE as date_of_report
		from patm.ybqtaweek qw
		inner join patm.ybqta qq
			on qq.compn = qw.compn and qq.slsno = qw.slsno and qq.prdcat = qw.prdcat
		inner join jfidata.territory_mappings tm
	    	on qw.slsno = tm.child_territory
		where qw.prdcat not in ('TABLES','RUGS')
	    group by tm.parent_territory,tm.name, tm.child_territory,qw.prdcat,qq.qtayr
	    order by territory,subterritory asc
    	) AS q 
    ON (d.date_of_report = q.date_of_report AND d.SubTerritory = q.SubTerritory AND d.category = q.prdcat and d.Territory = q.Territory and d.rep_name = q.rep_name)
    WHEN MATCHED THEN
    	UPDATE SET  d.daily_total = q.dailytotal,
    				d.weekly_quota = q.weekQuota
    WHEN NOT MATCHED THEN
    	INSERT (Territory,
				Rep_Name,
				Subterritory,
				category,
				daily_total,
				weekly_quota,
				date_of_report)
		VALUES (q.territory,
				q.rep_name,
				q.subterritory,
				q.prdcat,
				q.dailyTotal,
				q.weekQuota,
				q.date_of_report)
";

$prepBuild = odbc_prepare($DB2conn, $buildMerge);
$executeBuild = odbc_execute($prepBuild);

if($executeBuild){
	echo "Successfully Merged";
}else{
	echo "failed" . odbc_errormsg();
}



$selectForJSON = 
    "
    SELECT 
    TERRITORY,
    SUM(CASE WHEN CATEGORY = 'RECLINERS' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS RECLINERS_DAILY,
    SUM(CASE WHEN CATEGORY = 'RECLINERS' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS RECLINERS_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'RECLINERS' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'RECLINERS' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS RECLINER_PERCENT,
    SUM(CASE WHEN CATEGORY = 'LIFT-CHAIR' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS LIFT_CHAIR_DAILY,
    SUM(CASE WHEN CATEGORY = 'LIFT-CHAIR' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS LIFT_CHAIR_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'LIFT-CHAIR' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'LIFT-CHAIR' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS LIFT_CHAIR_PERCENT,
    SUM(CASE WHEN CATEGORY = 'PW-HDR-RCL' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS PW_HDR_RCL_DAILY,
    SUM(CASE WHEN CATEGORY = 'PW-HDR-RCL' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS PW_HDR_RCL_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'PW-HDR-RCL' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'PW-HDR-RCL' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS PW_HDR_RCL_PERCENT,
    SUM(CASE WHEN CATEGORY = 'LEATH-MOT' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS LEATH_MOT_DAILY,
    SUM(CASE WHEN CATEGORY = 'LEATH-MOT' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS LEATH_MOT_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'LEATH-MOT' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'LEATH-MOT' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS LEATH_MOT_PERCENT,
    SUM(CASE WHEN CATEGORY = 'FABRI-MOT' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS FABRI_MOT_DAILY,
    SUM(CASE WHEN CATEGORY = 'FABRI-MOT' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS FABRI_MOT_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'FABRI-MOT' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'FABRI-MOT' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS FABRI_MOT_PERCENT,
    SUM(CASE WHEN CATEGORY = 'STAT-LEATH' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS STAT_LEATH_DAILY,
    SUM(CASE WHEN CATEGORY = 'STAT-LEATH' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS STAT_LEATH_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'STAT-LEATH' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'STAT-LEATH' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS STAT_LEATH_PERCENT,
    SUM(CASE WHEN CATEGORY = 'STAT-FABRI' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS STAT_FABRI_DAILY,
    SUM(CASE WHEN CATEGORY = 'STAT-FABRI' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS STAT_FABRI_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'STAT-FABRI' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'STAT-FABRI' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS STAT_FABRI_PERCENT,
    SUM(CASE WHEN CATEGORY = 'POWER-MOT' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS POWER_MOT_DAILY,
    SUM(CASE WHEN CATEGORY = 'POWER-MOT' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS POWER_MOT_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'POWER-MOT' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'POWER-MOT' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS POWER_MOT_PERCENT,
    SUM(CASE WHEN CATEGORY = 'PW-HDR-MOT' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS PW_HDR_MOT_DAILY,
    SUM(CASE WHEN CATEGORY = 'PW-HDR-MOT' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS PW_HDR_MOT_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'PW-HDR-MOT' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'PW-HDR-MOT' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS PW_HDR_MOT_PERCENT,
    SUM(CASE WHEN CATEGORY = 'POWER-REC' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN DAILY_TOTAL ELSE 0 END) AS POWER_REC_DAILY,
    SUM(CASE WHEN CATEGORY = 'POWER-REC' AND  DATE_OF_REPORT >=  CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) AS POWER_REC_QUOTA,
    ( SUM(CASE WHEN CATEGORY = 'POWER-REC' THEN DAILY_TOTAL ELSE 0 END) * 100 ) / ( SUM(CASE WHEN CATEGORY = 'POWER-REC' AND  DATE_OF_REPORT >=  
        CURRENT DATE  THEN WEEKLY_QUOTA ELSE 0 END) ) AS POWER_REC_PERCENT,
    ROUND((CAST(SUM(DAILY_TOTAL) AS DECIMAL)/CAST(SUM(WEEKLY_QUOTA) AS DECIMAL)) * 100, 2) AS OVERALL,
    CURRENT_DATE  AS DATE_OF_REPORT
FROM JFIDATA.AMBITION_YELLOWBAR_METRICS
    WHERE DATE_OF_REPORT >= ( (CURRENT DATE ) - ( DAYOFWEEK(CURRENT DATE ) - 1 ) DAYS )
GROUP BY TERRITORY
    ";


//new array specifically for the final JSON file
$content2 = array();

$result = odbc_exec($DB2conn, $selectForJSON);

//creating array for new fetch since it now has the updated extension IDs
while ($d2 = odbc_fetch_array($result)) {


    //Store the current row
    $content2[] = $d2;
    echo $d2;

     }


// // Store it all into our final JSON file
file_put_contents('salesYellowbarLog.json', json_encode($content2, JSON_PRETTY_PRINT ));


//Beginning code to upload to Ambition API via PDOStatement

$url = 'https://jacksonfurniture.ambition.com/api/v1/data/file_upload_6dfbb82c_575d_402f_8870_50bc320b5fa6_depot/';
$token = '9dbbdff724bfa5b265fa5fa4c952d9ea115a6ec1';

//Initiate CURL
$ch = curl_init($url);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_HTTPHEADER => array(
    'Authorization: Token '.$token,
    'Content-Type : application/json'
    ),
CURLOPT_POSTFIELDS => json_encode($content2, JSON_PRETTY_PRINT)
     ));

//Execute request
$postResult = curl_exec($ch);

// Check for errors
if($postResult === FALSE){
    die(curl_error($ch));
    }

// Decode the response
$responseData = json_decode($postResult, TRUE);

$DB2conn = null;
$DB2dev = null;

?>