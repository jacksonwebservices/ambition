<?php
require 'vendor/autoload.php';
require 'phpmailer/vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
// use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpExcel\worksheet;
use PhpOffice\PhpExcel\Writer\Excel2007;
use PhpOffice\PhpExcel\Style\Alignment;
use PhpOffice\PhpExcel\Style\Border;
use PhpOffice\PhpExcel\Style\Borders;
use PhpOffice\PhpExcel\Style\Color;
use PhpOffice\PhpExcel\Style\Fill;
use PhpOffice\PhpExcel\Style\Font;


/*DATABASE CONNECTIONS*/

$servername = "192.168.20.11";
$username = "normanh";
$password = "Guitar138*";
$dbName = "Ambition";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbName);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

///////////////////////////////////////////////////////////////////////////////////

/*STYLE ARRAYS*/
// $styleArray = array(
//     'font' => array(
//         'bold' => true,
//         //'color' => array('rgb' => 'F0FF33'),
//     ),
//     'alignment' => array(
//         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
//     ),
//     'borders' => array(
//         'top' => array(
//             'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
//         ),
//     ),
//     'fill' => array(
//         'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
//         'rotation' => 90,
//         'startColor' => array(
//             'rgb' => 'F0FF33',
//         ),
//         'endColor' => array(
//             'argb' => 'F0FF33',
//         ),
//     ),
// );

// $styleArray2 = array(
//     'font' => array(
//         'bold' => true,
//         //'color' => array('rgb' => 'FFD133'),
//     ),
//     'alignment' => array(
//         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
//     ),
//     'borders' => array(
//         'top' => array(
//             'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
//         ),
//     ),
//     'fill' => array(
//         'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
//         'rotation' => 90,
//         'startColor' => array(
//             'rgb' => 'FFD133',
//         ),
//         'endColor' => array(
//             'argb' => 'FFD133',
//         ),
//     ),
// );

// $styleArray3 = array(
//     'font' => array(
//         'bold' => true,
//         //'color' => array('rgb' => 'FF9933'),
//     ),
//     'alignment' => array(
//         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
//     ),
//     'borders' => array(
//         'top' => array(
//             'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
//         ),
//     ),
//     'fill' => array(
//         'type' => PHPExcel_Style_Fill::FILL_SOLID,
//         'rotation' => 90,
//         'startColor' => array(
//             'argb' => 'FF9933',
//         ),
//         'endColor' => array(
//             'argb' => 'FF9933',
//         ),
//     ),
// );

// $styleArray4 = array(
//     'font' => array(
//         'bold' => true,
//         //'color' => array('rgb' => 'FF7133'),
//     ),
//     'alignment' => array(
//         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
//     ),
//     'borders' => array(
//         'top' => array(
//             'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
//         ),
//     ),
//     'fill' => array(
//         'type' => PHPExcel_Style_Fill::FILL_SOLID,
//         'rotation' => 90,
//         'startColor' => array(
//             'argb' => 'FF7133',
//         ),
//         'endColor' => array(
//             'argb' => 'FF7133',
//         ),
//     ),
// );

// $styleArray5 = array(
//     'font' => array(
//         'bold' => true,
//     ),
//     'alignment' => array(
//         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
//     ),
//     'borders' => array(
//         'top' => array(
//             'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
//         ),
//     ),
//     'fill' => array(
//         'type' => PHPExcel_Style_Fill::FILL_SOLID,
//         'rotation' => 90,
//         'startColor' => array(
//             'argb' => 'FFFFFFFF',
//         ),
//         'endColor' => array(
//             'argb' => 'FFFFFFFF',
//         ),
//     ),
// );

//////////////////////////////////////////////////


//Create and run query
$sql = "
    SELECT CONCAT(u.first_name, '  ', u.last_name) as Name,
           t.ext_id as ID,
           t.total_talk_time_minutes as TalkTime,
           t.total_outbound as Outbound,
           t.total_inbound as Inbound,
           t.dealers_contacted as Dealers,
           t.date_of_report as Date,
           DAYNAME(t.date_of_report) as Day
    FROM ambition.ambition_totals t
    INNER JOIN ambition.ambition_users u
      ON t.extension = u.extension
    WHERE date_of_report  between
        curdate() - interval 5 day and curdate()
      -- '2018-02-05' and '2018-02-09'
    ORDER BY ID, Date asc";

$sql2 = " 
	 SELECT 
           round(sum(t.total_talk_time_minutes),2) as TalkTime,
           sum(t.total_outbound) as Outbound,
           sum(t.total_inbound) as Inbound,
           sum(t.dealers_contacted) as Dealers,
           t.date_of_report as Date,
           DAYNAME(t.date_of_report) as Day
    FROM ambition.ambition_totals t
    WHERE date_of_report  between
         curdate() - interval 5 day and curdate()
        -- '2018-02-05' and '2018-02-09'
        group by Date
        order by Date";

$result = mysqli_query($conn,$sql);
$result2 = mysqli_query($conn,$sql2);
				 
//Start the spreadsheet
// $spreadsheet = new spreadsheet();
// $sheet = $spreadsheet->getActiveSheet();

$phpExcel = new PHPExcel;
$sheet = $phpExcel->getActiveSheet();
$sheet ->setTitle("Individual Totals");
$sheet ->getColumnDimension('A') -> setAutoSize(true);
$sheet ->getColumnDimension('B') -> setAutoSize(true);
$sheet ->getColumnDimension('C') -> setAutoSize(true);
$sheet ->getColumnDimension('D') -> setAutoSize(true);
$sheet ->getColumnDimension('E') -> setAutoSize(true);
$sheet ->getColumnDimension('F') -> setAutoSize(true);
$sheet ->getColumnDimension('G') -> setAutoSize(true);
$sheet ->getColumnDimension('H') -> setAutoSize(true);
$sheet ->getColumnDimension('I') -> setAutoSize(true);
$sheet ->getColumnDimension('J') -> setAutoSize(true);
$sheet ->getColumnDimension('K') -> setAutoSize(true);
$sheet ->getColumnDimension('L') -> setAutoSize(true);
$sheet ->getColumnDimension('M') -> setAutoSize(true);
$sheet ->getColumnDimension('N') -> setAutoSize(true);
$sheet ->getColumnDimension('O') -> setAutoSize(true);
$sheet ->getColumnDimension('P') -> setAutoSize(true);
$sheet ->getColumnDimension('Q') -> setAutoSize(true);


//Create header row
$sheet->setCellValue('A1', 'Name');
$sheet->setCellValue('B1', 'User ID');
$sheet->setCellValue('C1', 'Talk Time');
$sheet->setCellValue('F1', 'Outbound');
$sheet->setCellValue('I1', 'Inbound');
$sheet->setCellValue('L1', 'Dealers');
$sheet->setCellValue('O1', 'Date');
$sheet->setCellValue('P1', 'Day');

// $sheet ->getStyle("C1")->applyFromArray($styleArray);
// $sheet ->getStyle("F1")->applyFromArray($styleArray2);
// $sheet ->getStyle("I1")->applyFromArray($styleArray3);
// $sheet ->getStyle("L1")->applyFromArray($styleArray4);
// $sheet ->getStyle("A1:B1")->applyFromArray($styleArray5);
// $sheet ->getStyle("D1:E1")->applyFromArray($styleArray5);
// $sheet ->getStyle("G1:H1")->applyFromArray($styleArray5);
// $sheet ->getStyle("J1:K1")->applyFromArray($styleArray5);
// $sheet ->getStyle("M1:P1")->applyFromArray($styleArray5);


$sheet ->getStyle("C1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("C1")->getFill()->getStartColor()->setARGB('F0FF33');
$sheet ->getStyle("F1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("F1")->getFill()->getStartColor()->setARGB('FFD133');
$sheet ->getStyle("I1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("I1")->getFill()->getStartColor()->setARGB('FF9933');
$sheet ->getStyle("L1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("L1")->getFill()->getStartColor()->setARGB('FF7133');
$sheet ->getStyle("A1:B1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("A1:B1")->getFill()->getStartColor()->setARGB('FFFFFF');
$sheet ->getStyle("D1:E1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("D1:E1")->getFill()->getStartColor()->setARGB('FFFFFF');
$sheet ->getStyle("G1:H1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("G1:H1")->getFill()->getStartColor()->setARGB('FFFFFF');
$sheet ->getStyle("J1:K1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("J1:K1")->getFill()->getStartColor()->setARGB('FFFFFF');
$sheet ->getStyle("M1:P1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet ->getStyle("M1:P1")->getFill()->getStartColor()->setARGB('FFFFFF');

$sheet ->freezePane('C2');


// //Preprocess the data into a multi-dimensional array
// //  with the id as the parent index
$resultData = array();

while($row = mysqli_fetch_assoc($result)) 

	{
	   	$resultData[$row['ID']][] = $row;
	}
				 
//Set starting row number
$rowNo = 1;

// //Iterate over the results for each unique id
foreach($resultData as $idRecords)
	{
		 $userFirstRow = $rowNo+1;

	    //Iterate over the records for this ID
	    foreach($idRecords as $record)
		    {
		        //Increment row number
		        $rowNo++;
		        //Add record row to spreadsheet
		        $sheet->setCellValue("A{$rowNo}", $record['Name']);
		        $sheet->setCellValue("B{$rowNo}", $record['ID']);
		        $sheet->setCellValue("C{$rowNo}", $record['TalkTime']);
		        $sheet->setCellValue("F{$rowNo}", $record['Outbound']);
		        $sheet->setCellValue("I{$rowNo}", $record['Inbound']);
		        $sheet->setCellValue("L{$rowNo}", $record['Dealers']);
		        $sheet->setCellValue("O{$rowNo}", $record['Date']);
		        $sheet->setCellValue("P{$rowNo}", $record['Day']);

		    }

	    //Increment row number
	    $rowNo++;

	    $sheet->setCellValue("C{$rowNo}", "Mean");
	    $sheet->setCellValue("D{$rowNo}", "Median");
	    $sheet->setCellValue("E{$rowNo}", "Mode");
	    $sheet ->getStyle("C{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("C{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');
        $sheet ->getStyle("D{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("D{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');
        $sheet ->getStyle("E{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("E{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');

        $sheet->setCellValue("F{$rowNo}", "Mean");
	    $sheet->setCellValue("G{$rowNo}", "Median");
	    $sheet->setCellValue("H{$rowNo}", "Mode");
	    $sheet ->getStyle("F{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("F{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');
        $sheet ->getStyle("G{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("G{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');
        $sheet ->getStyle("H{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("H{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');

        $sheet->setCellValue("I{$rowNo}", "Mean");
	    $sheet->setCellValue("J{$rowNo}", "Median");
	    $sheet->setCellValue("K{$rowNo}", "Mode");
	    $sheet ->getStyle("I{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("I{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');
        $sheet ->getStyle("J{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("J{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');
        $sheet ->getStyle("K{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("K{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');

        $sheet->setCellValue("L{$rowNo}", "Mean");
	    $sheet->setCellValue("M{$rowNo}", "Median");
	    $sheet->setCellValue("N{$rowNo}", "Mode");
	    $sheet ->getStyle("L{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("L{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');
        $sheet ->getStyle("M{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("M{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');
        $sheet ->getStyle("N{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("N{$rowNo}")->getFill()->getStartColor()->setARGB('1aff1a');

		$rowNo++;

		$range = 'C'.$userFirstRow.':C'.($rowNo-1);
		$sheet->setCellValue("C{$rowNo}", '=AVERAGE('.$range.')');
		$sheet->setCellValue("D{$rowNo}", '=MEDIAN('.$range.')');
		$sheet->setCellValue("E{$rowNo}", '=MODE('.$range.')');
		$sheet ->getStyle("C{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("C{$rowNo}")->getFill()->getStartColor()->setARGB('F0FF33');
        $sheet ->getStyle("D{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("D{$rowNo}")->getFill()->getStartColor()->setARGB('F0FF33');
        $sheet ->getStyle("E{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("E{$rowNo}")->getFill()->getStartColor()->setARGB('F0FF33');



		$range2 = 'F'.$userFirstRow.':F'.($rowNo-1);
		$sheet->setCellValue("F{$rowNo}", '=AVERAGE('.$range2.')');
		$sheet->setCellValue("G{$rowNo}", '=MEDIAN('.$range2.')');
		$sheet->setCellValue("H{$rowNo}", '=MODE('.$range2.')');
		$sheet ->getStyle("F{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("F{$rowNo}")->getFill()->getStartColor()->setARGB('FFD133');
        $sheet ->getStyle("G{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("G{$rowNo}")->getFill()->getStartColor()->setARGB('FFD133');
        $sheet ->getStyle("H{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("H{$rowNo}")->getFill()->getStartColor()->setARGB('FFD133');



		$range3 = 'I'.$userFirstRow.':I'.($rowNo-1);
		$sheet->setCellValue("I{$rowNo}", '=AVERAGE('.$range3.')');
		$sheet->setCellValue("J{$rowNo}", '=MEDIAN('.$range3.')');
		$sheet->setCellValue("K{$rowNo}", '=MODE('.$range3.')');
		$sheet ->getStyle("I{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("I{$rowNo}")->getFill()->getStartColor()->setARGB('FF9933');
        $sheet ->getStyle("J{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("J{$rowNo}")->getFill()->getStartColor()->setARGB('FF9933');
        $sheet ->getStyle("K{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("K{$rowNo}")->getFill()->getStartColor()->setARGB('FF9933');



		$range4 = 'L'.$userFirstRow.':L'.($rowNo-1);
		$sheet->setCellValue("L{$rowNo}", '=AVERAGE('.$range4.')');
		$sheet->setCellValue("M{$rowNo}", '=MEDIAN('.$range4.')');
		$sheet->setCellValue("N{$rowNo}", '=MODE('.$range4.')');
		$sheet ->getStyle("L{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("L{$rowNo}")->getFill()->getStartColor()->setARGB('FF7133');
        $sheet ->getStyle("M{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("M{$rowNo}")->getFill()->getStartColor()->setARGB('FF7133');
        $sheet ->getStyle("N{$rowNo}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $sheet ->getStyle("N{$rowNo}")->getFill()->getStartColor()->setARGB('FF7133');

	}

var_dump($resultData);

/*BUILDING 2ND WORKSHEET FOR DEPARTMENT TOTALS*/
$worksheet1 = $phpExcel->createSheet();
$worksheet1->setTitle('Department Total');
$worksheet1 ->getColumnDimension('A') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('B') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('C') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('D') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('E') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('F') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('G') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('H') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('I') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('J') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('K') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('L') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('M') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('N') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('O') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('P') -> setAutoSize(true);
$worksheet1 ->getColumnDimension('Q') -> setAutoSize(true);


$worksheet1 ->getStyle("A1:C1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$worksheet1 ->getStyle("A1:C1")->getFill()->getStartColor()->setARGB('F0FF33');
$worksheet1 ->getStyle("D1:F1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$worksheet1 ->getStyle("D1:F1")->getFill()->getStartColor()->setARGB('FFD133');
$worksheet1 ->getStyle("G1:I1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$worksheet1 ->getStyle("G1:I1")->getFill()->getStartColor()->setARGB('FF9933');
$worksheet1 ->getStyle("J1:L1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$worksheet1 ->getStyle("J1:L1")->getFill()->getStartColor()->setARGB('FF7133');

//Headers
$worksheet1->setCellValue('A1', 'Talk Time');
$worksheet1->setCellValue('D1', 'Outbound');
$worksheet1->setCellValue('G1', 'Inbound');
$worksheet1->setCellValue('J1', 'Dealers');
$worksheet1->setCellValue('M1', 'Date');
$worksheet1->setCellValue('N1', 'Day');

$resultData2 = array();

while($row2 = mysqli_fetch_assoc($result2)) 

	{
		$resultData2[$row2[0]][] = $row2;
	}
		 
//Set starting row number
$rowNo2 = 1;

// //Iterate over the results for each unique id
foreach($resultData2 as $idRecords2)
	{
		 $userFirstRow2 = $rowNo2+1;

	    //Iterate over the records for this ID
	    foreach($idRecords2 as $record2)
		    {
		        //Increment row number
		        $rowNo2++;
		        //Add record row to spreadsheet
		        $worksheet1->setCellValue("A{$rowNo2}", $record2['TalkTime']);
		        $worksheet1->setCellValue("D{$rowNo2}", $record2['Outbound']);
		        $worksheet1->setCellValue("G{$rowNo2}", $record2['Inbound']);
		        $worksheet1->setCellValue("J{$rowNo2}", $record2['Dealers']);
		        $worksheet1->setCellValue("M{$rowNo2}", $record2['Date']);
		        $worksheet1->setCellValue("N{$rowNo2}", $record2['Day']);

		    }

	    //Increment row number
	   
		$rowNo++;

	}
	 $rowNo2++;

	    $worksheet1->setCellValue("A{$rowNo2}", "Mean");
	    $worksheet1->setCellValue("B{$rowNo2}", "Median");
	    $worksheet1->setCellValue("C{$rowNo2}", "Mode");
	    $worksheet1 ->getStyle("A{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("A{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');
        $worksheet1 ->getStyle("B{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("B{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');
        $worksheet1 ->getStyle("C{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("C{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');

        $worksheet1->setCellValue("D{$rowNo2}", "Mean");
	    $worksheet1->setCellValue("E{$rowNo2}", "Median");
	    $worksheet1->setCellValue("F{$rowNo2}", "Mode");
	    $worksheet1 ->getStyle("D{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("D{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');
        $worksheet1 ->getStyle("E{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("E{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');
        $worksheet1 ->getStyle("F{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("F{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');

        $worksheet1->setCellValue("G{$rowNo2}", "Mean");
	    $worksheet1->setCellValue("H{$rowNo2}", "Median");
	    $worksheet1->setCellValue("I{$rowNo2}", "Mode");
	    $worksheet1 ->getStyle("G{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("G{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');
        $worksheet1 ->getStyle("H{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("H{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');
        $worksheet1 ->getStyle("I{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("I{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');

        $worksheet1->setCellValue("J{$rowNo2}", "Mean");
	    $worksheet1->setCellValue("K{$rowNo2}", "Median");
	    $worksheet1->setCellValue("L{$rowNo2}", "Mode");
	    $worksheet1 ->getStyle("J{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("J{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');
        $worksheet1 ->getStyle("K{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("K{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');
        $worksheet1 ->getStyle("L{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("L{$rowNo2}")->getFill()->getStartColor()->setARGB('1aff1a');

        $rowNo2++;

        $deptRnge = 'A'.$userFirstRow2.':A'.($rowNo2-1);
		$worksheet1->setCellValue("A{$rowNo2}", '=AVERAGE('.$deptRnge.')');
		$worksheet1->setCellValue("B{$rowNo2}", '=MEDIAN('.$deptRnge.')');
		$worksheet1->setCellValue("C{$rowNo2}", '=MODE('.$deptRnge.')');
		$worksheet1 ->getStyle("A{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("A{$rowNo2}")->getFill()->getStartColor()->setARGB('F0FF33');
        $worksheet1 ->getStyle("B{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("B{$rowNo2}")->getFill()->getStartColor()->setARGB('F0FF33');
        $worksheet1 ->getStyle("C{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("C{$rowNo2}")->getFill()->getStartColor()->setARGB('F0FF33');

		$deptRnge2 = 'D'.$userFirstRow2.':D'.($rowNo2-1);
		$worksheet1->setCellValue("D{$rowNo2}", '=AVERAGE('.$deptRnge2.')');
		$worksheet1->setCellValue("E{$rowNo2}", '=MEDIAN('.$deptRnge2.')');
		$worksheet1->setCellValue("F{$rowNo2}", '=MODE('.$deptRnge2.')');
		$worksheet1 ->getStyle("D{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("D{$rowNo2}")->getFill()->getStartColor()->setARGB('FFD133');
        $worksheet1 ->getStyle("E{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("E{$rowNo2}")->getFill()->getStartColor()->setARGB('FFD133');
        $worksheet1 ->getStyle("F{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("F{$rowNo2}")->getFill()->getStartColor()->setARGB('FFD133');



		$deptRnge3 = 'G'.$userFirstRow2.':G'.($rowNo2-1);
		$worksheet1->setCellValue("G{$rowNo2}", '=AVERAGE('.$deptRnge3.')');
		$worksheet1->setCellValue("H{$rowNo2}", '=MEDIAN('.$deptRnge3.')');
		$worksheet1->setCellValue("I{$rowNo2}", '=MODE('.$deptRnge3.')');
		$worksheet1 ->getStyle("G{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("G{$rowNo2}")->getFill()->getStartColor()->setARGB('FF9933');
        $worksheet1 ->getStyle("H{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("H{$rowNo2}")->getFill()->getStartColor()->setARGB('FF9933');
        $worksheet1 ->getStyle("I{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("I{$rowNo2}")->getFill()->getStartColor()->setARGB('FF9933');



		$deptRnge4 = 'J'.$userFirstRow2.':J'.($rowNo2-1);
		$worksheet1->setCellValue("J{$rowNo2}", '=AVERAGE('.$deptRnge4.')');
		$worksheet1->setCellValue("K{$rowNo2}", '=MEDIAN('.$deptRnge4.')');
		$worksheet1->setCellValue("L{$rowNo2}", '=MODE('.$deptRnge4.')');
		$worksheet1 ->getStyle("J{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("J{$rowNo2}")->getFill()->getStartColor()->setARGB('FF7133');
        $worksheet1 ->getStyle("K{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("K{$rowNo2}")->getFill()->getStartColor()->setARGB('FF7133');
        $worksheet1 ->getStyle("L{$rowNo2}") ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $worksheet1 ->getStyle("L{$rowNo2}")->getFill()->getStartColor()->setARGB('FF7133');



//var_dump($resultData2);
	

// $writer = new xlsx($spreadsheet);
// $writer->save('Coaching Report - Test.xlsx');

					$writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
					$writer->save('Coaching Report - Weekly.xlsx');

  //       @ob_start();
  //       $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
		// $writer->save('php://output');
		// $data = @ob_get_contents();
		// @ob_end_clean();

					mysqli_close($conn);

					$mail = new PHPMailer(true);
					$address = "smedlin@jacksonfurnind.com";
					//$address = "hnorman@jacksonfurnind.com";

					$date = date("D M d, Y");


					try{
					$mail->setFrom("hnorman@jacksonfurnind.com");
					$mail->addAddress($address);
					$mail->AddAttachment('Coaching Report - Weekly.xlsx');
					$mail->isHTML(true);
					$mail->Subject    = "Weekly Coaching Report";
					$mail->Body       = "Attached is the weekly coaching report for " . $date;
					$mail->Send();
					echo 'message sent';

					} catch (Exception $e){
						echo 'message failed';
						echo 'mail error:' . $mail->ErrorInfo;
					}

?>

